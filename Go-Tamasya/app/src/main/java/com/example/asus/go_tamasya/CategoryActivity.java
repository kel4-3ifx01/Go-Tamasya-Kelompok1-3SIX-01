package com.example.asus.go_tamasya;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class CategoryActivity extends AppCompatActivity {

    TextView tv_pantai, tv_situs, tv_belanja, tv_kbinatang, tv_sport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        tv_pantai = (TextView) findViewById(R.id.tv_pantai);
        tv_situs = (TextView) findViewById(R.id.tv_situs);
        tv_belanja = (TextView) findViewById(R.id.tv_belanja);
        tv_kbinatang = (TextView) findViewById(R.id.tv_kbinatang);
        tv_sport = (TextView) findViewById(R.id.tv_sport);

    }

    public void pantai(View v){
        Intent intent = new Intent(CategoryActivity.this, DetailCategoryActivity.class);
        startActivity(intent);
    }

    public void situs(View v){
//        Toast.makeText(this, "replace ListTempatActivity", Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(CategoryActivity.this, ListTempatActivity.class);
//        startActivity(intent);
    }

    public void belanja(View v){
//        Toast.makeText(this, "replace ListTempatActivity", Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(CategoryActivity.this, ListTempatActivity.class);
//        startActivity(intent);
    }

    public void kbinatang(View v){
//        Toast.makeText(this, "replace ListTempatActivity", Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(CategoryActivity.this, ListTempatActivity.class);
//        startActivity(intent);
    }

    public void sport(View v){
//        Toast.makeText(this, "replace ListTempatActivity", Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(CategoryActivity.this, ListTempatActivity.class);
//        startActivity(intent);
    }

}
