package com.example.asus.go_tamasya;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.app.Activity;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class DetailCategoryActivity extends AppCompatActivity {

    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_category);

        list = (ListView) findViewById(R.id.list);

        final String[] itemname = {
                "Pulau Kanawa, Flores",
                "Pulau Moyo, NTB",
                "Pulau Bintan, Riau",
                "Pulau Burung, Belitung",
                "Tanjung Aan, Lombok"
        };

        final Integer[] imgid = {
                R.drawable.kanawa,
                R.drawable.moyo,
                R.drawable.bintan,
                R.drawable.pburung,
                R.drawable.tanjungaan
        };


        CustomListAdapter adapter = new CustomListAdapter(this, itemname, imgid);
        list.setAdapter(adapter);


        list.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
//                String Slecteditem = itemname[+position];
//                Toast.makeText(getApplicationContext(), Slecteditem, Toast.LENGTH_SHORT).show();
                startActivity( new Intent(DetailCategoryActivity.this, DetailWisataActivity.class) );

            }
        });
    }
}
