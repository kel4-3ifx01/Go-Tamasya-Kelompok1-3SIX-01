package com.example.asus.go_tamasya;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void order_click(View view){
        startActivity( new Intent(MenuActivity.this, OrderActivity.class) );
    }

    public void feedback_click(View view){
        startActivity( new Intent(MenuActivity.this, FeedbackActivity.class) );
    }

    public void kategori_click(View view){
        startActivity( new Intent(MenuActivity.this, CategoryActivity.class) );
    }


}
