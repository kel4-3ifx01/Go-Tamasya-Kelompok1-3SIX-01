package com.example.asus.go_tamasya;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class FeedbackActivity extends AppCompatActivity {

    RatingBar ratingBar;
    TextView deskrisi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

//        //layout
        deskrisi = (TextView) findViewById(R.id.deskrisi);
//        ratingBar = (RatingBar) findViewById(R.id.rating);
//        ratingBar.setRating(5);

        //notifikasi
        deskrisi.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Toast.makeText(FeedbackActivity.this, "Terimakasih telah menggunaan layanan Go-Tamasya", Toast.LENGTH_SHORT).show();
            }
        });

    }




}
