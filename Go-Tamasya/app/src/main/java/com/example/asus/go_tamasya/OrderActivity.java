package com.example.asus.go_tamasya;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class OrderActivity extends AppCompatActivity {

    TextView tv_car;
    TextView tv_minibus;
    TextView tv_motor;
    ImageView iv_map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        tv_car= (TextView) findViewById(R.id.tv_car);
        tv_minibus = (TextView) findViewById(R.id.tv_minibus);
        tv_motor = (TextView) findViewById(R.id.tv_motor);

        iv_map = (ImageView)findViewById(R.id.iv_mapp);
        iv_map.setVisibility(View.INVISIBLE);

    }

    public void car_click(View view){
        tv_car.setEnabled(false);
        tv_minibus.setEnabled(true);
        tv_motor.setEnabled(true);
    }
    public void minibus_click(View view){
        tv_minibus.setEnabled(false);
        tv_car.setEnabled(true);
        tv_motor.setEnabled(true);
    }
    public void motor_click(View view){
        tv_motor.setEnabled(false);
        tv_minibus.setEnabled(true);
        tv_car.setEnabled(true);
    }


    public void lokasi_click(View view){
        Toast.makeText(this, "aktifkan GPS", Toast.LENGTH_SHORT).show();
    }

    public void gps_click(View view){
        iv_map.setVisibility(View.VISIBLE);
    }

    public void next(View view){
        startActivity( new Intent(OrderActivity.this, PreConfirmActivity.class) );
    }

}
