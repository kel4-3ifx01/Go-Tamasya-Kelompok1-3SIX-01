package com.example.asus.go_tamasya;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class DetailWisataActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_wisata);
    }

    public void ulasan_click(View view) {
        startActivity(new Intent(DetailWisataActivity.this, UlasanActivity.class));
    }
}
