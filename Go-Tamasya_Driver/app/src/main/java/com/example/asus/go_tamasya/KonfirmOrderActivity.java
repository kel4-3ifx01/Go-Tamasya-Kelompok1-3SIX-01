package com.example.asus.go_tamasya;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Arrays;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class KonfirmOrderActivity extends AppCompatActivity {


    private ListView mainListView ;
    private ArrayAdapter<String> listAdapter ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konfirm_order);
        // Find the ListView resource.
        mainListView = (ListView) findViewById( R.id.mainListView );
        mainListView.setVisibility(View.INVISIBLE);

        // Create and populate a List of planet names.
        String[] planets = new String[] { "Dadang \n Jl. Babakansari - Jl. Merdeka", "Dini \n Jl. Buahbatu - Jl. Baleendah", "Ardi \n Jl. Ujunberung - Jl. Parakansaat"};
        ArrayList<String> planetList = new ArrayList<String>();
        planetList.addAll( Arrays.asList(planets) );

        // Create ArrayAdapter using the planet list.
        listAdapter = new ArrayAdapter<String>(this, R.layout.simplerow, planetList);

        // Add more planets. If you passed a String[] instead of a List<String>
        // into the ArrayAdapter constructor, you must not add more items.
        // Otherwise an exception will occur.
//        listAdapter.add( "Dimas" );
//        listAdapter.add( "Pluto" );
//        listAdapter.add( "Haumea" );
//        listAdapter.add( "Makemake" );
//        listAdapter.add( "Eris" );

        // Set the ArrayAdapter as the ListView's adapter.
        mainListView.setAdapter( listAdapter );
    }

    public void cari_click(View view){
        mainListView.setVisibility(View.VISIBLE);
    }

}
